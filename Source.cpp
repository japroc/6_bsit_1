#include <windows.h>
#include <stdio.h>
#include <Lm.h>
#include <locale>
#include <Sddl.h>
#include <Ntsecapi.h>
#include <vector>
#include <cstring>
#include <iostream>

#pragma comment(lib, "Netapi32.lib")

int(__stdcall * func_LsaOpenPolicy)(PLSA_UNICODE_STRING, PLSA_OBJECT_ATTRIBUTES, ACCESS_MASK, PLSA_HANDLE);
int(__stdcall * func_LsaEnumerateAccountRights)(LSA_HANDLE, PSID, PLSA_UNICODE_STRING, PULONG);
int(__stdcall * func_NetUserSetInfo)(LPCWSTR, LPCWSTR, DWORD, LPBYTE, LPDWORD);
int(__stdcall * func_NetUserGetLocalGroups)(LPCWSTR, LPCWSTR, DWORD, DWORD, LPBYTE, DWORD, LPDWORD, LPDWORD);
int(__stdcall * func_NetLocalGroupAdd)(LPCWSTR, DWORD, LPBYTE, LPDWORD);
int(__stdcall * func_NetLocalGroupDel)(LPCWSTR, LPCWSTR);
int(__stdcall * func_NetUserAdd)(LMSTR, DWORD, LPBYTE, LPDWORD);
int(__stdcall * func_NetUserDel)(LPCWSTR, LPCWSTR);


void GetFunctionAddresses()
{
	//-------------
	// Advapi32.dll
	// Netapi32.dll

	HMODULE dll_Advapi32 = LoadLibraryA("Advapi32.dll");
	if (dll_Advapi32 == NULL)
	{
		printf("Error : GetFunctionAddresses : LoadLibraryA : %d", GetLastError());
		exit(1);
	}

	HMODULE dll_Netapi32 = LoadLibraryA("Netapi32.dll");
	if (dll_Netapi32 == NULL)
	{
		printf("Error : GetFunctionAddresses : LoadLibraryA : %d", GetLastError());
		exit(1);
	}

	//-----------
	// NetUserDel

	func_NetUserDel = (int(__stdcall *)(LPCWSTR, LPCWSTR))
		GetProcAddress(dll_Netapi32, "NetUserDel");
	if (func_NetUserDel == NULL)
	{
		printf("Error : GetUsers : GetProcAddress : %d", GetLastError());
		exit(1);
	}

	//-----------
	// NetUserAdd

	func_NetUserAdd = (int(__stdcall *)(LMSTR, DWORD, LPBYTE, LPDWORD))
		GetProcAddress(dll_Netapi32, "NetUserAdd");
	if (func_NetUserAdd == NULL)
	{
		printf("Error : GetUsers : GetProcAddress : %d", GetLastError());
		exit(1);
	}

	//-----------------
	// NetLocalGroupDel

	func_NetLocalGroupDel = (int(__stdcall *)(LPCWSTR, LPCWSTR))
		GetProcAddress(dll_Netapi32, "NetLocalGroupDel");
	if (func_NetLocalGroupDel == NULL)
	{
		printf("Error : GetUsers : GetProcAddress : %d", GetLastError());
		exit(1);
	}

	//--------------
	// NetLocalGroupAdd

	func_NetLocalGroupAdd = (int(__stdcall *)(LPCWSTR, DWORD, LPBYTE, LPDWORD))
		GetProcAddress(dll_Netapi32, "NetLocalGroupAdd");
	if (func_NetLocalGroupAdd == NULL)
	{
		printf("Error : GetUsers : GetProcAddress : %d", GetLastError());
		exit(1);
	}

	//--------------
	// NetUserGetLocalGroups

	func_NetUserGetLocalGroups = (int(__stdcall *)(LPCWSTR, LPCWSTR, DWORD, DWORD, LPBYTE, DWORD, LPDWORD, LPDWORD))
		GetProcAddress(dll_Netapi32, "NetUserGetLocalGroups");
	if (func_NetUserGetLocalGroups == NULL)
	{
		printf("Error : GetUsers : GetProcAddress : %d", GetLastError());
		exit(1);
	}

	//--------------
	// LsaOpenPolicy

	func_LsaOpenPolicy = (int(__stdcall *)(PLSA_UNICODE_STRING, PLSA_OBJECT_ATTRIBUTES, ACCESS_MASK, PLSA_HANDLE))
		GetProcAddress(dll_Advapi32, "LsaOpenPolicy");
	if (func_LsaOpenPolicy == NULL)
	{
		printf("Error : GetUsers : GetProcAddress : %d", GetLastError());
		exit(1);
	}

	//--------------------------
	// LsaEnumerateAccountRights

	func_LsaEnumerateAccountRights = (int(__stdcall *)(LSA_HANDLE, PSID, PLSA_UNICODE_STRING, PULONG))
		GetProcAddress(dll_Advapi32, "LsaEnumerateAccountRights");
	if (func_LsaEnumerateAccountRights == NULL)
	{
		printf("Error : GetUsers : GetProcAddress : %d", GetLastError());
		exit(1);
	}

	//--------------------------
	// NetUserSetInfo

	func_NetUserSetInfo = (int(__stdcall *)(LPCWSTR, LPCWSTR, DWORD, LPBYTE, LPDWORD))
		GetProcAddress(dll_Netapi32, "NetUserSetInfo");
	if (func_NetUserSetInfo == NULL)
	{
		printf("Error : GetUsers : GetProcAddress : %d", GetLastError());
		exit(1);
	}
}

struct user
{
	char * name;
	char * SID;

};

struct group
{
	char * name;
	char * SID;

};

void PrintHelp();
void PrintAllPrivs();

void PrintUsers(std::vector<user> Users);
void PrintGroups(std::vector<group> Groups);

void ShowUsersPrivsPrologue(std::vector<user> Users);
void ShowGroupsPrivsPrologue(std::vector<group> Groups);

//-----------------------
// Create / Delete
// User / Group
// functions declarations
//-----------------------

void CreateGroup();
void DeleteGroup(std::vector<group> Groups);
void CreateUser();
void DeleteUser(std::vector<user> Users);

//---------------------------
// Add and remove privilegies
// functions declarations
//---------------------------

void AddPrivs(char * stringsid);
void AddUserPrivsPrologue(std::vector<user> Users);
void AddGroupPrivsPrologue(std::vector<group> Groups);

void RemovePrivs(char * stringsid);
void RemoveUserPrivsPrologue(std::vector<user> Users);
void RemoveGroupPrivsPrologue(std::vector<group> Groups);

//-----------------------
// Converters definitions
//-----------------------

char * convert_wchar_to_char(wchar_t * str)
{
	size_t origsize = wcslen(str) + 1;
	const size_t newsize = origsize * 2;
	char *nstring = (char *)malloc(newsize);
	// Put a copy of the converted string into nstring  
	wcstombs_s(NULL, nstring, newsize, str, _TRUNCATE);
	return nstring;
}

wchar_t * convert_char_to_wchar(const char * orig)
{
	size_t newsize = strlen(orig) + 1;
	wchar_t * wcstring = new wchar_t[newsize];
	size_t convertedChars = 0;
	mbstowcs_s(&convertedChars, wcstring, newsize, orig, _TRUNCATE);

	return wcstring;
}

char * NameToStringSid(char * name)
{
	//--------
	// Get SID
	//--------

	PSID sid = NULL;						// malloc
	DWORD cbSid = 0;
	LPSTR ReferencedDomainName = NULL;		// malloc
	DWORD cchReferencedDomainName = 0;
	SID_NAME_USE eUse;
	int status = LookupAccountNameA(NULL, name, NULL, &cbSid, NULL, &cchReferencedDomainName, &eUse);
	sid = (PSID)malloc(cbSid);
	ReferencedDomainName = (LPSTR)malloc(cchReferencedDomainName);
	status = LookupAccountNameA(NULL, name, sid, &cbSid, ReferencedDomainName, &cchReferencedDomainName, &eUse);
	if (status == 0)
	{
		printf("Error : NameToStringSid : LookupAccountNameA : %d", GetLastError());
		exit(1);
	}

	//---------------
	// Get SID string
	//---------------

	LPSTR SidString;
	status = ConvertSidToStringSidA(sid, &SidString);		// Convert Sid to StringSid
	if (status == 0)
	{
		printf("Error : NameToStringSid : ConvertSidToStringSidA : %d", GetLastError());
		exit(1);
	}

	//------------
	// Free memory
	//------------

	// LocalFree(SidString);
	free(ReferencedDomainName);
	free(sid);

	//-----------------
	// Return StringSid

	return SidString;
}

//-----------------------
// Password manipulations
//-----------------------

void ChangeUserPassword(std::vector<user> Users)
{
	int status;
	wchar_t * password = NULL;
	//---------------
	// Get user index

	int idx;
	std::cout << "Enter user index to change his password >>> ";
	std::cin >> idx;
	wchar_t * username = convert_char_to_wchar(Users[idx].name);

	//------------------
	// Read new password

	std::string pass;
	std::cout << "Enter new password >>> ";
	std::cin >> pass;

	//-----------------
	// Set new password

	USER_INFO_1003 ui1003;
	if (pass.compare("NONE") == 0)
		ui1003.usri1003_password = NULL;
	else
	{
		password = convert_char_to_wchar(pass.c_str());
		ui1003.usri1003_password = password;
	}
	
	//--------------
	// Set user info

	status = func_NetUserSetInfo(NULL, username, 1003, (LPBYTE)&ui1003, NULL);
	free(username);
	if (password) free(password);

	//-------------
	// Handle error

	switch (status)
	{
	case 0:
		std::cout << "Password updated to " <<  password << std::endl;
		break;
	default:
		std::cout << "Password was not updated." << std::endl;
	}

	return;
}

void ShowUserPassword(std::vector<user> Users)
{
	int status;

	//---------------
	// Get user index

	int idx;
	std::cout << "Enter user index to change his password >>> ";
	std::cin >> idx;

	//-------------
	// Get password

	PUSER_INFO_1 pui1;
	wchar_t * username = convert_char_to_wchar(Users[idx].name);
	status = NetUserGetInfo(NULL, username, 1, (LPBYTE *)&pui1);
	free(username);

	//----------------
	// Handle response

	switch (status)
	{
	case 0:

		if (pui1->usri1_password)
		{
			std::cout << Users[idx].name << " password : ";
			std::wcout << pui1->usri1_password << std::endl;
		}
		else
			std::wcout << Users[idx].name << " dont have a password." << std::endl;
		break;
	default:
		std::cout << "Can't get user password." << std::endl;
	}

	return;
}

//--------------------
// Users' and Groups'
// names manipulations
//--------------------

void ChangeUserName(std::vector<user> Users)
{
	int status;
	wchar_t * wusername = NULL;
	wchar_t * wnewname = NULL;

	//---------------
	// Get user index

	int idx;
	std::cout << "Enter user index to change name >>> ";
	std::cin >> idx;
	wusername = convert_char_to_wchar(Users[idx].name);

	//------------------
	// Read new username

	std::string newname;
	std::cout << "Enter new username >>> ";
	std::cin >> newname;

	//---------------------------
	// Create user_info structure

	USER_INFO_0 ui0;
	wnewname = ui0.usri0_name = convert_char_to_wchar(newname.c_str());
	
	//----------------
	// Change the name

	status = func_NetUserSetInfo(NULL, wusername, 0, (LPBYTE)&ui0, NULL);

	//--------------
	// Handle status

	switch (status)
	{
	case 0:
		std::cout << "Username was successfully updated." << std::endl;
		break;
	default:
		std::cout << "Username was not updated. " << std::endl;
	}

	//------------
	// free memory
	//  and return
	//------------

	free(wusername);
	free(wnewname);

	return;
}

void ChangeGroupName(std::vector<group> Groups)
{
	int status;
	wchar_t * w_oldname = NULL;
	wchar_t * w_newname = NULL;

	//---------------
	// Get user index

	int idx;
	std::cout << "Enter group index to change name >>> ";
	std::cin >> idx;
	w_oldname = convert_char_to_wchar(Groups[idx].name);

	//------------------
	// Read new username

	std::string s_newname;
	std::cout << "Enter new name >>> ";
	std::cin >> s_newname;

	//---------------------------
	// Create user_info structure

	LOCALGROUP_INFO_0 li0;
	w_newname = li0.lgrpi0_name = convert_char_to_wchar(s_newname.c_str());

	//----------------
	// Change the name

	status = NetLocalGroupSetInfo(NULL, w_oldname, 0, (LPBYTE)&li0, NULL);

	//--------------
	// Handle status

	switch (status)
	{
	case 0:
		std::cout << "Group name was successfully updated." << std::endl;
		break;
	default:
		std::cout << "Group name was not updated. " << std::endl;
	}

	//------------
	// free memory
	//  and return
	//------------

	free(w_oldname);
	free(w_newname);

	return;
}

//------------------------
//------------------------

void ShowUserGroups(std::vector<user> Users)
{
	int status;

	//---------------
	// Get user index

	int idx;
	std::cout << "Enter user index >>> ";
	std::cin >> idx;

	//-----------------
	// Get Local Groups

	wchar_t * w_uname = convert_char_to_wchar(Users[idx].name);
	PLOCALGROUP_USERS_INFO_0 lui0;
	DWORD entriesread;
	DWORD totalentries;

	status = NetUserGetLocalGroups(
		NULL,
		w_uname,
		0,
		0,
		(LPBYTE *)&lui0,
		MAX_PREFERRED_LENGTH,
		&entriesread,
		&totalentries);

	if (status != 0)
	{
		std::cout << "Error : ShowUserGroups : NetUserGetLocalGroups" << std::endl;
		return;
	}
	if (totalentries = 0)
	{
		std::cout << "There are no groups." << std::endl;
	}

	//------------------
	// Show local groups

	LPLOCALGROUP_USERS_INFO_0 pTmpBuf;
	DWORD i;
	DWORD dwTotalCount = 0;

	if ((pTmpBuf = lui0) != NULL)
	{
		// fprintf(stderr, "\nLocal group(s):\n");
		//
		// Loop through the entries and 
		//  print the names of the local groups 
		//  to which the user belongs. 
		//
		for (i = 0; i < entriesread; i++)
		{
			if (pTmpBuf == NULL)
			{
				fprintf(stderr, "An access violation has occurred\n");
				break;
			}

			wprintf(L"[*] %s\n", pTmpBuf->lgrui0_name);

			pTmpBuf++;
			dwTotalCount++;
		}
	}


	return;
}

void ShowUserTokenPrivs(std::vector<user> Users)
{
	int status;

	//------------------
	// Get user index
	// Get user password

	int idx;
	std::cout << "Enter user index >>> ";
	std::cin >> idx;

	std::string pass;
	std::cout << "Enter user password >>> ";
	std::cin >> pass;

	char * c_pass = NULL;
	if (pass.compare("NONE"))
	{
		c_pass = (char *)malloc(pass.length() + 1);
		strcpy(c_pass, pass.c_str());
	}

	//----------------
	// Get User Handle

	HANDLE hToken;
	status = LogonUserA(Users[idx].name, // 
		".",
		c_pass, //
		3, // LOGON32_LOGON_NETWORK,
		0, // LOGON32_PROVIDER_DEFAULT,
		&hToken);

	//------------------
	// GetToeknInformation

	DWORD dwLen = 0;
	status = GetTokenInformation(hToken, TokenPrivileges, NULL, 0, &dwLen);
	if (TRUE == status)
	{
		std::cout << "Error : ShowUserTokenPrivs : GetTokenInformation : GetLastError() = " << GetLastError() << std::endl;
		return;
	}

	LPVOID pBuffer = NULL;

	pBuffer = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwLen);
	if (NULL == pBuffer)
	{
		std::cout << "Error : ShowUserTokenPrivs : HeapAlloc : GetLastError() = " << GetLastError() << std::endl;
		return;
	}
	if (!GetTokenInformation(hToken, TokenPrivileges, pBuffer, dwLen, &dwLen))
	{
		//CloseHandle(hToken);
		HeapFree(GetProcessHeap(), 0, pBuffer);
		std::cout << "Error : ShowUserTokenPrivs : GetTokenInformation : GetLastError() = " << GetLastError() << std::endl;
		return;
	}

	//------------------------
	// Print holded privileges

	TOKEN_PRIVILEGES* pPrivs = NULL;
	pPrivs = (TOKEN_PRIVILEGES*)pBuffer;
	char * lpName = NULL;

	DWORD dwSize;
	for (DWORD iCount = 0; iCount < pPrivs->PrivilegeCount; iCount++)
	{
		//bStatus = LookupPrivilegeName(NULL, &(pPrivs->Privileges[iCount].Luid), lpName, &dwSize);
		lpName = (char *)malloc(200);
		status = LookupPrivilegeNameA(NULL, &(pPrivs->Privileges[iCount].Luid), lpName, &dwSize);
		if (status == 0)
		{
			//std::cout << "Error : ShowUserTokenPrivs : LookupPrivilegeName : GetLastError() = " << GetLastError() << std::endl;
			free(lpName);
			continue;
		}
		std::cout << "[*] " << lpName << std::endl;

		free(lpName);
	}
	
	return;
}

void GetPrivileges(char * stringsid)
{
	int status;
	//--------------------
	// Work with LSAPolicy
	// to get privileges
	//--------------------

	//-------------------
	// Open Policy Handle

	// LsaOpenPolicy dependencies
	LSA_OBJECT_ATTRIBUTES ObjectAttributes;
	memset(&ObjectAttributes, 0, sizeof(LSA_OBJECT_ATTRIBUTES));
	ObjectAttributes.Length = sizeof(LSA_OBJECT_ATTRIBUTES);
	LSA_HANDLE PolicyHandle;

	// Get PolicyHandle
	status = func_LsaOpenPolicy(
		NULL,
		&ObjectAttributes,
		//POLICY_LOOKUP_NAMES,
		POLICY_ALL_ACCESS,
		&PolicyHandle
		);

	if (status != 0)	// STATUS_SUCCESS
	{
		printf("Error : GetPrivileges : LsaOpenPolicy : %d", GetLastError());
		exit(1);
	}

	//--------
	// Get SID

	PSID sid;
	status = ConvertStringSidToSidA(stringsid, &sid);
	if (status == 0)
	{
		printf("Error : GetPrivileges : ConvertStringSidToSidA : %d", GetLastError());
		exit(1);
	}

	//---------------------
	// Enumerate Privileges

	PLSA_UNICODE_STRING UserRights = NULL;
	ULONG CountOfRights;

	status = LsaEnumerateAccountRights(
		PolicyHandle,
		sid,
		&UserRights,
		&CountOfRights);

	if (status != 0)
	{
		// printf("%u\n", status);
		// status = LsaNtStatusToWinError(status);
		// printf("Error : GetPrivileges : LsaEnumerateAccountRights : %d", GetLastError());
		// exit(1);
		std::cout << "There are no privileges." << std::endl;
		LsaClose(PolicyHandle);
		return;
	}
	
	//-----------------
	// Print privileges
	//-----------------

	for (DWORD i = 0; i < CountOfRights; ++i)
	{
		std::wcout << "[*] " << UserRights[i].Buffer << std::endl;
	}

	//----------
	// Close LSA
	LsaClose(PolicyHandle);
}

std::vector<user> GetUsers()
{

	//------------------------------------------
	// Get function to retrive the list of users
	//------------------------------------------

	HMODULE dll_Netapi32 = LoadLibraryA("Netapi32.dll");
	if (dll_Netapi32 == NULL)
	{
		printf("Error : GetUsers : LoadLibraryA : %d", GetLastError());
		exit(1);
	}

	int(__stdcall * func_NetUserEnum)(LPCWSTR, DWORD, DWORD, LPBYTE, DWORD, LPDWORD, LPDWORD, LPDWORD);
	func_NetUserEnum = (int(__stdcall *)(LPCWSTR, DWORD, DWORD, LPBYTE, DWORD, LPDWORD, LPDWORD, LPDWORD))
		GetProcAddress(dll_Netapi32, "NetUserEnum");
	if (func_NetUserEnum == NULL)
	{
		printf("Error : GetUsers : GetProcAddress : %d", GetLastError());
		exit(1);
	}

	//------------------
	// Get list of users
	//------------------

	LPUSER_INFO_0 ui0 = NULL;
	LPUSER_INFO_0 tmp_ui0 = NULL;
	DWORD  entries_read;
	DWORD  total_entries;
	int status = func_NetUserEnum(
		NULL,
		0,
		0x0002, // FILTER_NORMAL_ACCOUNT,
		(LPBYTE)&ui0,
		(DWORD)-1, //MAX_PREFERRED_LENGTH,
		&entries_read,
		&total_entries,
		NULL
		);

	//-------------------------------
	// Add all Users' names to vector
	//-------------------------------

	std::vector<user> Users;

	if ((tmp_ui0 = ui0) != NULL)
	{
		for (unsigned int i = 0; (i < entries_read); i++)
		{
			if (tmp_ui0 == NULL)
			{
				fprintf(stderr, "An access violation has occurred\n");
				break;
			}

			user new_entry;
			char * name = convert_wchar_to_char(tmp_ui0->usri0_name);
			new_entry.name = name;

			Users.push_back(new_entry);
			tmp_ui0++;
		}
	}

	//----------------
	// Get Users' SIDs
	//----------------

	for (unsigned int i = 0; i < entries_read; ++i)
	{
		Users[i].SID = NameToStringSid(Users[i].name);
	}

	//---------------------
	// Free memory and
	// Return list of users
	//---------------------

	// NetApiBufferFree(ui0);

	return Users;
}

std::vector<group> GetGroups()
{
	int status;

	//-------------------------------------------
	// Get function to retrive the list of groups
	//-------------------------------------------

	HMODULE dll_Netapi32 = LoadLibraryA("Netapi32.dll");
	if (dll_Netapi32 == NULL)
	{
		printf("Error : GetGroups : LoadLibraryA : %d", GetLastError());
		exit(1);
	}

	int(__stdcall * func_NetLocalGroupEnum)(LPCWSTR, DWORD, LPBYTE, DWORD, LPDWORD, LPDWORD, PDWORD_PTR);
	func_NetLocalGroupEnum = (int(__stdcall *)(LPCWSTR, DWORD, LPBYTE, DWORD, LPDWORD, LPDWORD, PDWORD_PTR))
		GetProcAddress(dll_Netapi32, "NetLocalGroupEnum");
	if (func_NetLocalGroupEnum == NULL)
	{
		printf("Error : GetGroups : GetProcAddress : %d", GetLastError());
		exit(1);
	}

	//------------------
	// Get list of users
	//------------------

	PLOCALGROUP_INFO_0 gi0, gi0_tmp;
	DWORD entriesread;
	DWORD totalentries;

	status = func_NetLocalGroupEnum(
		NULL,
		0,
		(LPBYTE)&gi0,
		MAX_PREFERRED_LENGTH,
		&entriesread,
		&totalentries,
		NULL);

	if (status != 0)
	{
		printf("Error : GetGroups : func_NetLocalGroupEnum : %d", GetLastError());
		exit(1);
	}

	//---------------------------
	// Create group vector
	// And fill it by group names
	//---------------------------

	std::vector<group> Groups;

	gi0_tmp = gi0;

	for (unsigned int i = 0; i < entriesread; i++)
	{
		group new_entry;
		char * name = convert_wchar_to_char(gi0_tmp->lgrpi0_name);
		new_entry.name = name;

		Groups.push_back(new_entry);
		gi0_tmp++;
	}

	//----------------
	// Get Groups' SIDs
	//----------------

	for (unsigned int i = 0; i < entriesread; ++i)
	{
		Groups[i].SID = NameToStringSid(Groups[i].name);
	}

	return Groups;
}

int main()
{
	setlocale(LC_ALL, "Russian");

	GetFunctionAddresses();

	std::vector<user> Users = GetUsers();
	std::vector<group> Groups = GetGroups();

	PrintHelp();
	while (true)
	{
		std::string cmd;
		std::cout << ">>> ";
		std::cin >> cmd;

		if (!cmd.compare("help"))
			PrintHelp();
		else if (!cmd.compare("show_users"))
		{
			Users = GetUsers();
			PrintUsers(Users);
		}
		else if (!cmd.compare("show_groups"))
		{
			Groups = GetGroups();
			PrintGroups(Groups);
		}
		else if (!cmd.compare("show_u_privs"))
		{
			ShowUsersPrivsPrologue(Users);
		}
		else if (!cmd.compare("show_g_privs"))
		{
			ShowGroupsPrivsPrologue(Groups);
		}
		else if (!cmd.compare("create_group"))
		{
			CreateGroup();
		}
		else if (!cmd.compare("delete_group"))
		{
			DeleteGroup(Groups);
		}
		else if (!cmd.compare("create_user"))
		{
			CreateUser();
		}
		else if (!cmd.compare("delete_user"))
		{
			DeleteUser(Users);
		}
		else if (!cmd.compare("add_u_privs"))
		{
			AddUserPrivsPrologue(Users);
		}
		else if (!cmd.compare("add_g_privs"))
		{
			AddGroupPrivsPrologue(Groups);
		}
		else if (!cmd.compare("show_privs"))
		{
			PrintAllPrivs();
		}
		else if (!cmd.compare("remove_u_privs"))
		{
			RemoveUserPrivsPrologue(Users);
		}
		else if (!cmd.compare("remove_g_privs"))
		{
			RemoveGroupPrivsPrologue(Groups);
		}
		else if (!cmd.compare("change_u_pass"))
			ChangeUserPassword(Users);
		/*
		else if (!cmd.compare("show_u_pass"))
			ShowUserPassword(Users);
		*/
		else if (!cmd.compare("change_u_name"))
			ChangeUserName(Users);
		else if (!cmd.compare("change_g_name"))
			ChangeGroupName(Groups);
		else if (!cmd.compare("show_u_groups"))
			ShowUserGroups(Users);
		else if (!cmd.compare("show_u_token_privs"))
			ShowUserTokenPrivs(Users);
		else if (!cmd.compare("show_u_groups"))
			ShowUserGroups(Users);
		else
			std::cout << "Unknown command" << std::endl;
	}
	

	printf("Done");

	return 0;
}

void PrintHelp()
{
	std::cout << "help - to show this message." << std::endl;

	std::cout << "show_users - to show local users." << std::endl;
	std::cout << "show_groups - to show local groups." << std::endl;

	std::cout << "show_privs - to show list of privilegies." << std::endl;
	std::cout << "show_u_privs - to show certain user's privileges." << std::endl;
	std::cout << "show_g_privs - to show certain group's privileges." << std::endl;

	std::cout << "create_group - to create new local group." << std::endl;
	std::cout << "delete_group - to delete existing local group." << std::endl;
	std::cout << "create_user - to create new local user." << std::endl;
	std::cout << "delete_user - to delete local user." << std::endl;

	std::cout << "add_u_privs - to add privileges to a user." << std::endl;
	std::cout << "add_g_privs - to add privileges to a group." << std::endl;
	std::cout << "remove_u_privs - to remove user privelege." << std::endl;
	std::cout << "remove_g_privs - to remove group privilege." << std::endl;

	std::cout << "change_u_pass - to change user password." << std::endl;
	// std::cout << "show_u_pass - to show specified user password." << std::endl;

	std::cout << "change_u_name - to change user name." << std::endl;
	std::cout << "change_g_name - to change group name." << std::endl;

	// std::cout << "show_u_token_privs - to show privs in token." << std::endl;
	std::cout << "show_u_groups - to show user groups." << std::endl;
}

void PrintUsers(std::vector<user> Users)
{
	int idx = 0;

	for (std::vector<user>::iterator iter = Users.begin(); iter != Users.end(); ++iter, ++idx)
	{
		std::cout << "[" << idx << "] " << iter->name  << "\n\tSID: " << iter->SID << std::endl;
	}

	return;
}

void PrintGroups(std::vector<group> Groups)
{
	int idx = 0;

	for (std::vector<group>::iterator iter = Groups.begin(); iter != Groups.end(); ++iter, ++idx)
	{
		std::cout << "[" << idx << "] " << iter->name << "\n\tSID: " << iter->SID  << std::endl;
	}

	return;
}

void ShowUsersPrivsPrologue(std::vector<user> Users)
{
	unsigned int idx;
	std::cout << "Enter number of user >>> ";
	std::cin >> idx;
	
	GetPrivileges(Users[idx].SID);

	return;
}

void ShowGroupsPrivsPrologue(std::vector<group> Groups)
{
	unsigned int idx;
	std::cout << "Enter number of group >>> ";
	std::cin >> idx;

	GetPrivileges(Groups[idx].SID);

	return;
}

//----------------------
// Create / Delete
// User / Group
// functions definitions
//----------------------

void CreateGroup()
{
	int status;

	//-------------------------
	// Get new local group name

	std::cout << "Enter new group name >>> ";
	std::string name;
	std::cin >> name;

	//---------------------
	// Add it to the struct
	LOCALGROUP_INFO_0 lgi0;
	lgi0.lgrpi0_name = convert_char_to_wchar(name.c_str());

	//-------------
	// Create group

	status = func_NetLocalGroupAdd(NULL, 0, (LPBYTE)&lgi0, NULL);
	if (status != 0)
	{
		printf("NET Error : CreateGroup : NetLocalGroupAdd : %d", status);
		exit(1);
	}

	return;
}

void DeleteGroup(std::vector<group> Groups)
{
	int status;

	//-----------------------------
	// Get index of group to delete

	std::cout << "Enter index of group to delete >>> ";
	int idx;
	std::cin >> idx;

	//-----------------------
	// Delete specified group

	wchar_t * groupname = convert_char_to_wchar(Groups[idx].name);
	status = func_NetLocalGroupDel(NULL, groupname);

	//------------------------
	// Handle some error codes

	switch (status)
	{
	// this case should never happen... just in case...
	case NERR_GroupNotFound:
		std::cout << "Specified group not found" << std::endl;
		break;
	case 0:
		std::cout << Groups[idx].name << " deleted." << std::endl;
		break;
	default:
		std::cout << "Group not deleted" << std::endl;
		break;
	}
}

void CreateUser()
{
	int status;

	//------------------
	// Get new user name

	std::string name;
	std::cout << "Enter new user name >>> ";
	std::cin >> name;

	//------------------------
	// Fill new user structure
	//------------------------

	// https://msdn.microsoft.com/en-us/library/windows/desktop/aa371109(v=vs.85).aspx
	USER_INFO_1 ui1;

	ui1.usri1_name = convert_char_to_wchar(name.c_str());
	ui1.usri1_password = NULL;
	ui1.usri1_password_age = NULL;
	ui1.usri1_priv = USER_PRIV_USER;
	ui1.usri1_home_dir = NULL;
	ui1.usri1_comment = NULL;
	ui1.usri1_flags = UF_PASSWD_NOTREQD | UF_NORMAL_ACCOUNT;
	ui1.usri1_script_path = NULL;

	//------------
	// Create User

	status = func_NetUserAdd(NULL, 1, (LPBYTE)&ui1, NULL);

	//------------------------
	// Handle some error codes

	switch (status)
	{
	case 0:
		std::cout << "New user created." << std::endl;
		break;
	case NERR_UserExists:
		std::cout << "A user with the same name already exists." << std::endl;
		break;
	case NERR_GroupExists:
		std::cout << "A group with the same name already exists." << std::endl;
		break;
	default:
		std::cout << "New user was not created (NERR : " << status << ")." << std::endl;
	}

	return;
}

void DeleteUser(std::vector<user> Users)
{
	int status;

	//---------------
	// Get user index

	int idx;
	std::cout << "Enter user to delete index >>> ";
	std::cin >> idx;

	//----------------
	// Delete the user
	wchar_t * username = convert_char_to_wchar(Users[idx].name);
	status = func_NetUserDel(NULL, username);
	free(username);

	//------------------------
	// Handle some error codes

	switch (status)
	{
	case 0:
		std::cout << "User deleted." << std::endl;
		break;
	case NERR_UserNotFound:
		std::cout << "The user name could not be found." << std::endl;
		break;
	default:
		std::cout << "User was not deleted (NERR : " << status << ")." << std::endl;
	}

	return;
}

//---------------------------
// Add and remove privilegies
//---------------------------

void PrintAllPrivs()
{
	// Account Rights Constants
	std::cout << "Rights:" << std::endl;

	std::cout << "[*] SeBatchLogonRight" << std::endl;
	std::cout << "[*] SeDenyBatchLogonRight" << std::endl;
	std::cout << "[*] SeDenyInteractiveLogonRight" << std::endl;
	std::cout << "[*] SeDenyNetworkLogonRight" << std::endl;
	std::cout << "[*] SeDenyRemoteInteractiveLogonRight" << std::endl;
	std::cout << "[*] SeDenyServiceLogonRight" << std::endl;
	std::cout << "[*] SeInteractiveLogonRight" << std::endl;
	std::cout << "[*] SeNetworkLogonRight" << std::endl;
	std::cout << "[*] SeRemoteInteractiveLogonRight" << std::endl;
	std::cout << "[*] SeServiceLogonRight" << std::endl;
	

	// Privilege Constants
	std::cout << "Privileges:" << std::endl;

	std::cout << "[*] SeAssignPrimaryTokenPrivilege" << std::endl;
	std::cout << "[*] SeAuditPrivilege" << std::endl;
	std::cout << "[*] SeBackupPrivilege" << std::endl;
	std::cout << "[*] SeChangeNotifyPrivilege" << std::endl;
	std::cout << "[*] SeCreateGlobalPrivilege" << std::endl;
	std::cout << "[*] SeCreatePagefilePrivilege" << std::endl;
	std::cout << "[*] SeCreatePermanentPrivilege" << std::endl;
	std::cout << "[*] SeCreateSymbolicLinkPrivilege" << std::endl;
	std::cout << "[*] SeCreateTokenPrivilege" << std::endl;
	std::cout << "[*] SeDebugPrivilege" << std::endl;
	std::cout << "[*] SeEnableDelegationPrivilege" << std::endl;
	std::cout << "[*] SeImpersonatePrivilege" << std::endl;
	std::cout << "[*] SeIncreaseBasePriorityPrivilege" << std::endl;
	std::cout << "[*] SeIncreaseQuotaPrivilege" << std::endl;
	std::cout << "[*] SeIncreaseWorkingSetPrivilege" << std::endl;
	std::cout << "[*] SeLoadDriverPrivilege" << std::endl;
	std::cout << "[*] SeLockMemoryPrivilege" << std::endl;
	std::cout << "[*] SeMachineAccountPrivilege" << std::endl;
	std::cout << "[*] SeManageVolumePrivilege" << std::endl;
	std::cout << "[*] SeProfileSingleProcessPrivilege" << std::endl;
	std::cout << "[*] SeRelabelPrivilege" << std::endl;
	std::cout << "[*] SeRemoteShutdownPrivilege" << std::endl;
	std::cout << "[*] SeRestorePrivilege" << std::endl;
	std::cout << "[*] SeSecurityPrivilege" << std::endl;
	std::cout << "[*] SeShutdownPrivilege" << std::endl;
	std::cout << "[*] SeSyncAgentPrivilege" << std::endl;
	std::cout << "[*] SeSystemEnvironmentPrivilege" << std::endl;
	std::cout << "[*] SeSystemProfilePrivilege" << std::endl;
	std::cout << "[*] SeSystemtimePrivilege" << std::endl;
	std::cout << "[*] SeTakeOwnershipPrivilege" << std::endl;
	std::cout << "[*] SeTcbPrivilege" << std::endl;
	std::cout << "[*] SeTimeZonePrivilege" << std::endl;
	std::cout << "[*] SeTrustedCredManAccessPrivilege" << std::endl;
	std::cout << "[*] SeUndockPrivilege" << std::endl;
	std::cout << "[*] SeUnsolicitedInputPrivilege" << std::endl;
}

//----
// Add

void AddPrivs(char * stringsid)
{
	int status;

	//---------------------------
	// LsaOpenPolicy dependencies

	LSA_OBJECT_ATTRIBUTES ObjectAttributes;
	memset(&ObjectAttributes, 0, sizeof(LSA_OBJECT_ATTRIBUTES));
	ObjectAttributes.Length = sizeof(LSA_OBJECT_ATTRIBUTES);
	LSA_HANDLE PolicyHandle;

	//-----------------
	// Get PolicyHandle

	status = LsaOpenPolicy(
		NULL,
		&ObjectAttributes,
		//POLICY_LOOKUP_NAMES,
		POLICY_ALL_ACCESS,
		&PolicyHandle
		);

	if (status != 0)	// STATUS_SUCCESS
	{
		printf("Error : GetPrivileges : LsaOpenPolicy : %d", GetLastError());
		exit(1);
	}

	//--------
	// Get SID

	PSID sid;
	status = ConvertStringSidToSidA(stringsid, &sid);
	if (status == 0)
	{
		printf("Error : GetPrivileges : ConvertStringSidToSidA : %d", GetLastError());
		exit(1);
	}

	//-----------------------------
	// Get privilege name to create

	std::string priv_name;
	std::cout << "Enter privilege name to add >>> ";
	std::cin >> priv_name;

	//---------------------------
	// Create privilege structure

	LSA_UNICODE_STRING lus;

	wchar_t * w_priv_name = convert_char_to_wchar(priv_name.c_str());
	lus.Buffer = w_priv_name;
	lus.Length = lus.MaximumLength = strlen(priv_name.c_str()) * 2;

	//----------------------------------------------
	// Add privilege to an entity with specified sid

	status = LsaAddAccountRights(PolicyHandle, sid, &lus, 1);

	//-------------------
	// Handle error codes

	switch (status)
	{
	case 0:
		std::cout << "Privilege added to the entity." << std::endl;
		break;
	// dont like this number
	// # define STATUS_NO_SUCH_PRIVILEGE ((NTSTATUS) 0xC0000060L)
	case 0xC0000060L:
		std::cout << "Specified privilege name is not correct." << std::endl;
		break;
	default:
		std::cout << "Privilege not added." << std::endl;
	}
	
	return;
}

void AddUserPrivsPrologue(std::vector<user> Users)
{
	//---------------
	// Get user index
	std::cout << "Enter user index >>> ";
	int idx;
	std::cin >> idx;

	AddPrivs(Users[idx].SID);

	return;
}

void AddGroupPrivsPrologue(std::vector<group> Groups)
{
	//---------------
	// Get user index
	std::cout << "Enter group index >>> ";
	int idx;
	std::cin >> idx;

	AddPrivs(Groups[idx].SID);

	return;
}

//-------
// Remove

void RemovePrivs(char * stringsid)
{
	int status;

	//---------------------------
	// LsaOpenPolicy dependencies

	LSA_OBJECT_ATTRIBUTES ObjectAttributes;
	memset(&ObjectAttributes, 0, sizeof(LSA_OBJECT_ATTRIBUTES));
	ObjectAttributes.Length = sizeof(LSA_OBJECT_ATTRIBUTES);
	LSA_HANDLE PolicyHandle;

	//-----------------
	// Get PolicyHandle

	status = LsaOpenPolicy(
		NULL,
		&ObjectAttributes,
		//POLICY_LOOKUP_NAMES,
		POLICY_ALL_ACCESS,
		&PolicyHandle
		);

	if (status != 0)	// STATUS_SUCCESS
	{
		printf("Error : GetPrivileges : LsaOpenPolicy : %d", GetLastError());
		exit(1);
	}

	//--------
	// Get SID

	PSID sid;
	status = ConvertStringSidToSidA(stringsid, &sid);
	if (status == 0)
	{
		printf("Error : GetPrivileges : ConvertStringSidToSidA : %d", GetLastError());
		exit(1);
	}

	//-----------------------------
	// Get privilege name to delete

	std::string priv_name;
	std::cout << "Enter privilege name to delete >>> ";
	std::cin >> priv_name;

	//---------------------------
	// Create privilege structure

	LSA_UNICODE_STRING lus;

	wchar_t * w_priv_name = convert_char_to_wchar(priv_name.c_str());
	lus.Buffer = w_priv_name;
	lus.Length = lus.MaximumLength = strlen(priv_name.c_str()) * 2;

	//----------------------------------------------
	// Add privilege to an entity with specified sid

	status = LsaRemoveAccountRights(PolicyHandle, sid, false, &lus, 1);

	//------------------------
	// Handle some error codes

	switch (status)
	{
	case 0:
		std::cout << "Privilege removed successfully." << std::endl;
		break;
	// dont like this number
	// #define STATUS_NO_SUCH_PRIVILEGE -1073741728
	case 0xC0000060:
		std::cout << "Privilege names is not valid." << std::endl;
		break;
	default:
		std::cout << "Privilege was not removed." << std::endl;
	}

	return;
}

void RemoveUserPrivsPrologue(std::vector<user> Users)
{
	//---------------
	// Get user index
	std::cout << "Enter user index >>> ";
	int idx;
	std::cin >> idx;

	RemovePrivs(Users[idx].SID);

	return;
}

void RemoveGroupPrivsPrologue(std::vector<group> Groups)
{
	//---------------
	// Get user index
	std::cout << "Enter group index >>> ";
	int idx;
	std::cin >> idx;

	RemovePrivs(Groups[idx].SID);

	return;
}
